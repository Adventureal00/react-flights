import React, { ChangeEvent, useCallback, useEffect, useState } from "react";
import styled from "styled-components";
import { useDebouncedCallback } from "use-debounce";

const InputWrapper = styled.div`
  line-height: 18px;
`;

interface FormInputProps {
  value: string | number;
  name?: string;
  id?: string;
  className?: string;
  onChange(e: ChangeEvent<HTMLInputElement>): void;
}

const FormInput: React.FC<FormInputProps> = (props) => {
  const [inputValue, setInputValue] = useState("");

  useEffect(() => {
    setInputValue(props.value.toString());
  }, [props.value]);

  const debouncedCallback = useDebouncedCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      if (props.onChange) {
        props.onChange(event);
      }
    },
    200
  );

  const onChangeHandler = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const newVal = event.target.value;
      setInputValue(newVal);
      debouncedCallback.callback(event);
    },
    [debouncedCallback]
  );

  return (
    <InputWrapper>
      <input {...props} value={inputValue} onChange={onChangeHandler} />
    </InputWrapper>
  );
};

export default FormInput;
