import flightsReducer, { IFlightsReducer } from "./flights";
import filtersReducer, { IFiltersReducer } from "./filters";

export interface IRootReducer {
  flights: IFlightsReducer;
  filters: IFiltersReducer;
}

const rootReducer: IRootReducer = {
  flights: flightsReducer,
  filters: filtersReducer,
};

export default rootReducer;
