import styled from "styled-components";
import { greyColor, lightBlackColor } from "../../../styles/colors";
import FormInput from "../../shared/forms/FormInput/FormInput";

const FormWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 35px 21px;
`;

const FormItem = styled.div`
  display: flex;
  flex-direction: column;

  &:not(:first-child) {
    margin-top: 25px;
  }
`;

const FormItemContent = styled.div`
  margin-top: 24px;
`;

const FormItemLabel = styled.label`
  font-weight: bold;
  color: ${lightBlackColor};
`;

const PriceItem = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex: 1;

  &:not(:first-child) {
    margin-top: 18px;
  }
`;

const PriceItemLabel = styled.span`
  margin-right: 3px;
  color: ${greyColor};
`;

const PriceItemInputWrapper = styled.div`
  flex: 1;
`;

const PriceItemInput = styled(FormInput)`
  width: 100%;
`;

export {
  FormWrapper,
  FormItem,
  FormItemLabel,
  FormItemContent,
  PriceItem,
  PriceItemInputWrapper,
  PriceItemInput,
  PriceItemLabel,
};
