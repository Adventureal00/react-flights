import { useFormik } from "formik";
import React, { useCallback, useEffect } from "react";
import {
  FormItem,
  FormItemContent,
  FormItemLabel,
  FormWrapper,
  PriceItem,
  PriceItemInput,
  PriceItemInputWrapper,
  PriceItemLabel,
} from "./styles";
import FormRadioList from "../../shared/forms/FormRadioList/FormRadioList";
import FormCheckboxList, {
  CheckboxListItemProps,
} from "../../shared/forms/FormCheckboxList/FormCheckboxList";

export interface IFiltersFormState {
  sortBy: string;
  filters: string[];
  price: {
    from: string;
    to: string;
  };
  airlines: string[];
}

interface FiltersFormProps {
  onFormChange(values: IFiltersFormState): void;
  companies: CheckboxListItemProps[];
}

const SORTS_HEADER = "Сортировать";
const FILTERS_HEADER = "Фильтровать";
const PRICE_HEADER = "Цены";
const AIRLINES_HEADER = "Авиакомпании";

const PRICE_FROM = "От";
const PRICE_TO = "До";

const SORTS_VALUES = [
  { value: "priceUp", label: "по возрастанию цены" },
  { value: "priceDown", label: "по убыванию цены" },
  { value: "time", label: "по времени пути" },
];
const FILTERS_VALUES = [
  { value: "noTransfer", label: "без пересадок" },
  { value: "oneTransfer", label: "1 пересадка" },
];

const FiltersForm: React.FC<FiltersFormProps> = ({
  onFormChange,
  companies,
}) => {
  const { values, setFieldValue, handleChange } = useFormik<IFiltersFormState>({
    initialValues: {
      sortBy: "time",
      filters: [],
      price: {
        from: "",
        to: "",
      },
      airlines: [],
    },
    onSubmit: submitForm,
  });

  function submitForm() {}

  useEffect(() => {
    onFormChange(values);
  }, [values, onFormChange]);

  const onFiltersChange = useCallback(
    (selected: string | string[]) => {
      if (Array.isArray(selected)) {
        setFieldValue("filters", selected);
      } else {
        setFieldValue("filters", [selected]);
      }
    },
    [setFieldValue]
  );

  const onSortsChange = useCallback(
    (selected: string) => {
      setFieldValue("sortBy", selected);
    },
    [setFieldValue]
  );

  const onCompaniesChange = useCallback(
    (selected: string[] | string) => {
      if (Array.isArray(selected)) {
        setFieldValue("airlines", selected);
      } else {
        setFieldValue("airlines", [selected]);
      }
    },
    [setFieldValue]
  );

  return (
    <FormWrapper>
      <FormItem>
        <FormItemLabel>{SORTS_HEADER}</FormItemLabel>
        <FormItemContent>
          <FormRadioList
            value={values.sortBy}
            onChange={onSortsChange}
            items={SORTS_VALUES}
          />
        </FormItemContent>
      </FormItem>
      <FormItem>
        <FormItemLabel>{FILTERS_HEADER}</FormItemLabel>
        <FormItemContent>
          <FormCheckboxList
            value={values.filters}
            onChange={onFiltersChange}
            multi={true}
            items={FILTERS_VALUES}
          />
        </FormItemContent>
      </FormItem>
      <FormItem>
        <FormItemLabel>{PRICE_HEADER}</FormItemLabel>
        <FormItemContent>
          <PriceItem>
            <PriceItemLabel>{PRICE_FROM}</PriceItemLabel>
            <PriceItemInputWrapper>
              <PriceItemInput
                value={values.price.from}
                name={"price.from"}
                onChange={handleChange}
              />
            </PriceItemInputWrapper>
          </PriceItem>
          <PriceItem>
            <PriceItemLabel>{PRICE_TO}</PriceItemLabel>
            <PriceItemInputWrapper>
              <PriceItemInput
                value={values.price.to}
                name={"price.to"}
                onChange={handleChange}
              />
            </PriceItemInputWrapper>
          </PriceItem>
        </FormItemContent>
      </FormItem>
      <FormItem>
        <FormItemLabel>{AIRLINES_HEADER}</FormItemLabel>
        <FormItemContent>
          <FormCheckboxList
            value={values.airlines}
            onChange={onCompaniesChange}
            multi={true}
            items={companies}
          />
        </FormItemContent>
      </FormItem>
    </FormWrapper>
  );
};

export default FiltersForm;
