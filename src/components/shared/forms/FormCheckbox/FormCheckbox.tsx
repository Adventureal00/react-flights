import React, { ChangeEvent, useState } from "react";
import { uniqueId } from "lodash";
import styled from "styled-components";
import {greyColor} from "../../../../styles/colors";

const CheckboxWrapper = styled.div``;

const CheckboxLabel = styled.label`
  margin-left: 4px;
  color: ${greyColor};
`;

interface FormCheckboxProps {
  title?: string;
  checked: boolean;
  value: string | number;
  onChange(e: ChangeEvent<HTMLInputElement>): void;
}

const FormCheckbox: React.FC<FormCheckboxProps> = (props) => {
  const [checkboxId] = useState(uniqueId("checkbox-").toString());
  return (
    <CheckboxWrapper>
      <input
        type={"checkbox"}
        id={checkboxId}
        value={props.value}
        onChange={props.onChange}
        checked={props.checked}
      />
      {props.title && (
        <CheckboxLabel htmlFor={checkboxId}>- {props.title}</CheckboxLabel>
      )}
    </CheckboxWrapper>
  );
};

export default FormCheckbox;
