import styled from "styled-components";

const LayoutContainer = styled.div`
  max-width: 1190px;
  margin: 0 auto;
`;

export default LayoutContainer;
