import { action, Action } from "easy-peasy";
import { IFiltersFormState } from "../../components/Filters/FiltersForm/FiltersForm";

export interface IFiltersReducer {
  data: {
    filterBy: string[];
    sortBy: string;
    price: {
      from: number;
      to: number;
    };
    airlines: string[];
  };
  setFilters: Action<IFiltersReducer, IFiltersFormState>;
}

const filtersReducer: IFiltersReducer = {
  data: {
    filterBy: [],
    sortBy: "",
    price: {
      from: 0,
      to: 0,
    },
    airlines: [],
  },
  setFilters: action((state, payload) => {
    state.data = {
      price: {
        from: parseInt(payload.price.from),
        to: parseInt(payload.price.to),
      },
      sortBy: payload.sortBy,
      filterBy: payload.filters,
      airlines: payload.airlines,
    };
  }),
};

export default filtersReducer;
