import React from "react";
import FiltersForm, {
  IFiltersFormState,
} from "../../Filters/FiltersForm/FiltersForm";
import { CheckboxListItemProps } from "../../shared/forms/FormCheckboxList/FormCheckboxList";

interface FlightFiltersProps {
  onFiltersFormChange(values: IFiltersFormState): void;
  companies: CheckboxListItemProps[];
}

const FlightFilters: React.FC<FlightFiltersProps> = (props) => {
  return (
    <FiltersForm
      companies={props.companies}
      onFormChange={props.onFiltersFormChange}
    />
  );
};

export default FlightFilters;
