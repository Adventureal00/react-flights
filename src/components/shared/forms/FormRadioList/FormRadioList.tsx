import React, { ChangeEvent, useCallback, useEffect, useMemo } from "react";
import styled from "styled-components";
import FormRadio from "../FormRadio/FormRadio";
import useSelectedList from "../../../../hooks/selectedList";

const RadioList = styled.ul`
  list-style-type: none;
`;

const RadioListItem = styled.li`
  padding: 4px 0;
`;

export interface RadioListItemProps {
  value: string;
  label: string;
}

interface FormRadioListProps {
  items: RadioListItemProps[];
  value?: string;
  onChange(selected: string): void;
}

const FormRadioList: React.FC<FormRadioListProps> = ({
  items,
  onChange,
  value,
}) => {
  const { selected, addSelected } = useSelectedList<string>(
    value ? value : undefined,
    false
  );

  useEffect(() => {
    if (selected) {
      onChange(selected.toString());
    }
  }, [selected, onChange]);

  const onRadioChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      addSelected(e.target.value);
    },
    [addSelected]
  );

  const listContent = useMemo(() => {
    return items.map((item) => (
      <RadioListItem key={item.value}>
        <FormRadio
          title={item.label}
          checked={item.value === selected}
          onChange={onRadioChange}
          value={item.value}
        />
      </RadioListItem>
    ));
  }, [items, selected, onRadioChange]);

  return <RadioList>{listContent}</RadioList>;
};

export default FormRadioList;
