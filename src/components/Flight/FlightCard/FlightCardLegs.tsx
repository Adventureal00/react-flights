import React, { useMemo } from "react";
import { IFlightLeg } from "../../../models/IFlightSegment";
import moment from "moment";
import ContentDivider from "../../shared/ContentDivider/ContentDivider";
import { lightBlueColor } from "../../../styles/colors";
import { BsArrowRight } from "react-icons/bs";
import { MdAccessTime } from "react-icons/md";
import {
  DateTimeDate,
  DateTimeTime,
  LegsList,
  MarginRightContent,
  SegmentAirportCode,
  SegmentCompany,
  SegmentPlaces,
  SegmentPlacesIcon,
  SegmentPlacesItem,
  SegmentTimeline,
  SegmentWrapper,
  TimeIcon,
  TimelineDateTime,
  TimelineDateTimeReverse,
  TimelineIconTime,
} from "./styles";

interface FlightCardLegsProps {
  legs: IFlightLeg[];
}

const FlightCardLegs: React.FC<FlightCardLegsProps> = ({ legs }) => {
  const getFormattedDate = (data: string, format: string) => {
    return moment(data).format(format).toString();
  };

  const parsedDuration = (duration: number) => {
    const hours = Math.floor(duration / 60);
    const minutes = duration - hours * 60;

    return `${hours} ч ${minutes} мин`;
  };

  const getFlightCarrierString = (carrier: string) =>
    `Рейс выполняет ${carrier}`;
  const getPlaceItem = (city: string, airport: string) => {
    return `${city}, ${airport}`;
  };

  const legsContent = useMemo(() => {
    return legs.map((item, index) => {
      const departureSegment = item.segments[0];
      const arrivalSegment = item.segments[item.segments.length - 1];

      return (
        <SegmentWrapper key={index}>
          <SegmentPlaces>
            <SegmentPlacesItem>
              {departureSegment.departureCity &&
                getPlaceItem(
                  departureSegment.departureCity.caption,
                  departureSegment.departureAirport.caption
                )}
              <SegmentAirportCode>
                ({departureSegment.airline.airlineCode})
              </SegmentAirportCode>
            </SegmentPlacesItem>
            <SegmentPlacesIcon>
              <BsArrowRight color={lightBlueColor} />
            </SegmentPlacesIcon>
            <SegmentPlacesItem>
              {getPlaceItem(
                arrivalSegment.departureCity.caption,
                arrivalSegment.departureAirport.caption
              )}
              <SegmentAirportCode>
                ({arrivalSegment.airline.airlineCode})
              </SegmentAirportCode>
            </SegmentPlacesItem>
          </SegmentPlaces>
          <SegmentTimeline>
            <TimelineDateTime>
              <MarginRightContent>
                <DateTimeTime>
                  {getFormattedDate(departureSegment.departureDate, "HH:mm")}
                </DateTimeTime>
              </MarginRightContent>
              <DateTimeDate>
                {getFormattedDate(departureSegment.departureDate, "DD MMM ddd")}
              </DateTimeDate>
            </TimelineDateTime>
            <TimelineIconTime>
              <TimeIcon>
                <MdAccessTime />
              </TimeIcon>
              {parsedDuration(item.duration)}
            </TimelineIconTime>
            <TimelineDateTimeReverse>
              <DateTimeTime>
                {getFormattedDate(arrivalSegment.arrivalDate, "HH:mm")}
              </DateTimeTime>
              <MarginRightContent>
                <DateTimeDate>
                  {getFormattedDate(arrivalSegment.arrivalDate, "DD MMM ddd")}
                </DateTimeDate>
              </MarginRightContent>
            </TimelineDateTimeReverse>
          </SegmentTimeline>
          <ContentDivider
            showTitle={item.segments.length > 1}
            title={(item.segments.length - 1).toString() + " пересадка"}
          />
          <SegmentCompany>
            {getFlightCarrierString(arrivalSegment.airline.caption)}
          </SegmentCompany>
        </SegmentWrapper>
      );
    });
  }, [legs]);

  return <LegsList>{legsContent}</LegsList>;
};

export default FlightCardLegs;
