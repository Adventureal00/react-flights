import React, { ChangeEvent, useCallback, useEffect, useMemo } from "react";
import styled from "styled-components";
import FormCheckbox from "../FormCheckbox/FormCheckbox";
import useSelectedList from "../../../../hooks/selectedList";

const CheckboxList = styled.ul`
  list-style-type: none;
`;

const CheckboxListItem = styled.li`
  padding: 4px 0;
`;

export interface CheckboxListItemProps {
  value: string;
  label: string;
}

interface FormCheckboxListProps {
  items: CheckboxListItemProps[];
  multi?: boolean;
  value?: string | string[];
  onChange(selected: string[] | string): void;
}

const FormCheckboxList: React.FC<FormCheckboxListProps> = ({
  items,
  multi,
  onChange,
  value,
}) => {
  const { selected, addSelected } = useSelectedList<string>(getInitialState());

  function getInitialState() {
    if (value) {
      return value;
    } else if (multi) {
      return [];
    } else {
      return "";
    }
  }

  useEffect(() => {
    if (selected) {
      onChange(selected);
    }
  }, [selected, onChange]);

  const onCheckboxChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      addSelected(e.target.value);
    },
    [addSelected]
  );

  const listContent = useMemo(() => {
    return items.map((item) => (
      <CheckboxListItem key={item.value}>
        <FormCheckbox
          key={item.value}
          title={item.label}
          checked={selected?.indexOf(item.value) !== -1}
          value={item.value}
          onChange={onCheckboxChange}
        />
      </CheckboxListItem>
    ));
  }, [items, selected, onCheckboxChange]);

  return <CheckboxList>{listContent}</CheckboxList>;
};

export default FormCheckboxList;
