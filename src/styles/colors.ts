const blueColor = "#0087c9";
const orangeColor = "#ffb168";
const lightBlueColor = "#00a6e1";
const blackColor = "#1c1c1c";
const lightBlackColor = "#425668";
const greyColor = "#a2a2a2";
const lightGreyColor = "#e5e5e5";

export {
  blueColor,
  orangeColor,
  lightBlueColor,
  blackColor,
  greyColor,
  lightGreyColor,
  lightBlackColor,
};
