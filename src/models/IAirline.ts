import { IUidCaption } from "./IUidCaption";

export interface IAirline extends IUidCaption {
  airlineCode: string;
}
