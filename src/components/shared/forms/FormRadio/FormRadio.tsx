import React, { ChangeEvent, useState } from "react";
import styled from "styled-components";
import { uniqueId } from "lodash";
import {greyColor} from "../../../../styles/colors";

interface FormRadioProps {
  title?: string;
  checked: boolean;
  value: string | number;
  onChange(e: ChangeEvent<HTMLInputElement>): void;
}

const RadioWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const RadioLabel = styled.label`
  margin-left: 4px;
  color: ${greyColor};
`;

const FormRadio: React.FC<FormRadioProps> = (props) => {
  const [radioId] = useState(uniqueId("radio-").toString());

  return (
    <RadioWrapper>
      <input
        type={"radio"}
        id={radioId}
        checked={props.checked}
        value={props.value}
        onChange={props.onChange}
      />
      {props.title && (
        <RadioLabel htmlFor={radioId}>- {props.title}</RadioLabel>
      )}
    </RadioWrapper>
  );
};

export default FormRadio;
