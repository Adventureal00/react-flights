import React from "react";
import {
  FlightCardButton,
  FlightCardContent,
  FlightCardHeader,
  FlightCardWrapper,
  FlightCostSub,
  FlightCostTitle,
  HeaderDetails,
} from "./styles";
import FlightCardLegs from "./FlightCardLegs";
import { IFlightLeg } from "../../../models/IFlightSegment";

interface FlightCardProps {
  flightCost: number;
  flightLegs: IFlightLeg[];
}

const CARD_HEADER = "Стоимость для одного взрослого пассажира";
const CARD_BUTTON = "Выбрать";

const FlightCard: React.FC<FlightCardProps> = (props) => {
  const getCostTitle = () => {
    return `${props.flightCost} ₽`;
  };

  return (
    <FlightCardWrapper>
      <FlightCardHeader>
        <HeaderDetails>
          <FlightCostTitle>{getCostTitle()}</FlightCostTitle>
          <FlightCostSub>{CARD_HEADER}</FlightCostSub>
        </HeaderDetails>
      </FlightCardHeader>
      <FlightCardContent>
        <FlightCardLegs legs={props.flightLegs} />
      </FlightCardContent>
      <FlightCardButton>{CARD_BUTTON}</FlightCardButton>
    </FlightCardWrapper>
  );
};

export default FlightCard;
