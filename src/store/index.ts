import { createStore, createTypedHooks } from "easy-peasy";
import rootReducer, { IRootReducer } from "./reducers/root";

const typedHooks = createTypedHooks<IRootReducer>();

export const useStoreActions = typedHooks.useStoreActions;
export const useStoreState = typedHooks.useStoreState;

export default createStore(rootReducer);
