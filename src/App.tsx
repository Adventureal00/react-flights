import React from "react";
import LayoutContainer from "./components/shared/LayoutContainer/LayoutContainer";
import { GlobalStyle } from "./styles/main";
import styled from "styled-components";
import FlightFiltersContainer from "./components/Filters/FlightFiltersContainer/FlightFiltersContainer";
import FlightsContainerList from "./components/Flight/FlightsContainerList/FlightsContainerList";

const ContentWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

function App() {
  return (
    <>
      <GlobalStyle />
      <LayoutContainer>
        <ContentWrapper>
          <FlightFiltersContainer />
          <FlightsContainerList />
        </ContentWrapper>
      </LayoutContainer>
    </>
  );
}

export default App;
