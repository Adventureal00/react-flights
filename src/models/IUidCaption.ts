export interface IUidCaption {
  uid: string;
  caption: string;
}
