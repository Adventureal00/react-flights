import styled from "styled-components";
import {
  blackColor,
  blueColor,
  lightBlueColor,
  lightGreyColor,
  orangeColor,
} from "../../../styles/colors";

export const FlightCardWrapper = styled.div`
  color: ${blackColor};
  width: 640px;
`;

export const FlightCardHeader = styled.div`
  display: flex;
  background: ${blueColor};
  justify-content: flex-end;
  padding: 8px 16px;
`;

export const HeaderDetails = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  color: #fff;
  font-weight: bold;
`;

export const FlightCostTitle = styled.span`
  font-size: 18px;
`;
export const FlightCostSub = styled.span`
  font-size: 12px;
  font-weight: 500;
`;

export const FlightCardContent = styled.div`
  padding: 0 8px;
`;

export const FlightCardButton = styled.button`
  background: ${orangeColor};
  display: flex;
  justify-content: center;
  align-items: center;
  color: #fff;
  text-transform: uppercase;
  padding: 14px 0;
  border: none;
  font-weight: bold;
  width: 100%;
  
  &:hover{
    cursor: pointer;
    opacity: .8;
  }
`;

export const SegmentWrapper = styled.div`
  width: 100%;
  border-bottom: 2px solid ${blueColor};
  padding-bottom: 13px;

  &:last-child {
    border-bottom: none;
  }
`;

export const SegmentPlaces = styled.div`
  display: flex;
  align-items: center;
  border-bottom: 1px solid ${lightGreyColor};
  padding: 10px 12px;
`;

export const SegmentPlacesItem = styled.div``;

export const SegmentPlacesIcon = styled.div`
  padding: 0 6px;
  display: flex;
  align-items: center;
`;

export const SegmentAirportCode = styled.span`
  color: ${lightBlueColor};
`;

export const SegmentTimeline = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 15px;
  padding: 0 12px;
`;

export const TimelineDateTime = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-end;
`;

export const TimelineDateTimeReverse = styled(TimelineDateTime)`
  flex-direction: row-reverse;
`;

export const TimelineIconTime = styled.div`
  display: flex;
`;

export const TimeIcon = styled.div`
  margin-right: 6px;
`;

export const DateTimeTime = styled.span`
  font-size: 16px;
`;

export const DateTimeDate = styled.span`
  color: ${lightBlueColor};
  font-size: 14px;
`;

export const LegsList = styled.ul`
  list-style-type: none;
`;

export const MarginRightContent = styled.div`
  margin-right: 6px;
`;

export const SegmentCompany = styled.span`
  margin-top: 20px;
  font-size: 14px;
  padding: 0 12px;
`;
