import { IFlight } from "../../models/IFlight";
import { action, Action, computed, Computed } from "easy-peasy";
import flights from "../../flights.json";
import { IRootReducer } from "./root";
import { IAirline } from "../../models/IAirline";

interface JSONDataType {
  result: {
    flights: IFlight[];
  };
}

export interface IFlightsReducer {
  flights: IFlight[];
  setFlights: Action<IFlightsReducer, IFlight[]>;
  flightsCompanies: Computed<IFlightsReducer, IAirline[]>;
  flightsCompaniesPrices: Computed<
    IFlightsReducer,
    { value: IAirline; price: number }[]
  >;
  filtersFlights: Computed<IFlightsReducer, IFlight[], IRootReducer>;
}

const flightsReducer: IFlightsReducer = {
  flights: (flights as JSONDataType).result.flights,
  setFlights: action((state, payload) => {
    state.flights = payload;
  }),
  flightsCompanies: computed((state) => {
    const foundedIds = new Set();
    return state.flights
      .filter((item) => {
        if (foundedIds.has(item.flight.carrier.uid)) return false;
        else {
          foundedIds.add(item.flight.carrier.uid);
          return true;
        }
      })
      .map((item) => item.flight.carrier);
  }),
  flightsCompaniesPrices: computed((state) => {
    const returnArr: { value: IAirline; price: number }[] = [];
    state.flightsCompanies.forEach((item) => {
      const companiesFlights = state.flights.filter(
        (fItem) => fItem.flight.carrier.uid === item.uid
      );
      companiesFlights.sort((a, b) => {
        const aPrice = parseInt(a.flight.price.totalFeeAndTaxes.amount);
        const bPrice = parseInt(b.flight.price.totalFeeAndTaxes.amount);
        if (aPrice < bPrice) {
          return -1;
        }
        if (aPrice > bPrice) {
          return 1;
        }

        return 0;
      });
      const minimalPrice =
        companiesFlights.length > 0 && companiesFlights[0]
          ? parseInt(companiesFlights[0].flight.price.totalFeeAndTaxes.amount)
          : 0;
      returnArr.push({ value: item, price: minimalPrice });
    });

    return returnArr;
  }),
  filtersFlights: computed(
    [(state) => state.flights, (state, rootState) => rootState.filters.data],
    (flights, filters) => {
      const filterMinPrice = filters.price.from > 0;
      const filterMaxPrice = filters.price.to > 0;
      const filterNoTransfer = filters.filterBy.indexOf("noTransfer") !== -1;
      const filterOneTransfer = filters.filterBy.indexOf("oneTransfer") !== -1;
      const filterCompanies = filters.airlines.length > 0;
      const sort = filters.sortBy;

      return flights
        .filter((item) => {
          if (
            filterMinPrice &&
            !(
              parseInt(item.flight.price.totalFeeAndTaxes.amount) >
              filters.price.from
            )
          ) {
            return false;
          }
          if (
            filterMaxPrice &&
            !(
              parseInt(item.flight.price.totalFeeAndTaxes.amount) <
              filters.price.to
            )
          ) {
            return false;
          }

          if (filterOneTransfer && filterNoTransfer) {
            let isValid = true;
            item.flight.legs.forEach((item) => {
              if (item.segments.length > 3) {
                isValid = false;
              }
            });
            if (!isValid) {
              return false;
            }
          } else {
            if (filterNoTransfer) {
              let isValid = true;
              item.flight.legs.forEach((item) => {
                if (item.segments.length !== 1) {
                  isValid = false;
                }
              });
              if (!isValid) {
                return false;
              }
            }

            if (filterOneTransfer) {
              let isValid = true;
              item.flight.legs.forEach((item) => {
                if (item.segments.length !== 2) {
                  isValid = false;
                }
              });
              if (!isValid) {
                return false;
              }
            }
          }

          if (
            filterCompanies &&
            filters.airlines.indexOf(item.flight.carrier.uid) === -1
          ) {
            return false;
          }

          return true;
        })
        .sort((a, b) => {
          if (sort === "priceUp" || sort === "priceDown") {
            const aPrice = parseInt(a.flight.price.totalFeeAndTaxes.amount);
            const bPrice = parseInt(b.flight.price.totalFeeAndTaxes.amount);
            if (aPrice < bPrice) {
              if (sort === "priceUp") {
                return -1;
              } else {
                return 1;
              }
            }

            if (aPrice > bPrice) {
              if (sort === "priceUp") {
                return 1;
              } else {
                return -1;
              }
            }

            return 0;
          } else {
            let aDuration = 0;
            let bDuration = 0;
            a.flight.legs.forEach((item) => {
              aDuration += item.duration;
            });
            b.flight.legs.forEach((item) => {
              bDuration += item.duration;
            });

            if (aDuration < bDuration) {
              return -1;
            }

            if (aDuration > bDuration) {
              return 1;
            }

            return 0;
          }
        });
    }
  ),
};

export default flightsReducer;
