import React, { useCallback, useMemo } from "react";
import { useStoreActions, useStoreState } from "../../../store";
import FlightFilters from "../../Flight/FlightFilters/FlightFilters";
import { IFiltersFormState } from "../FiltersForm/FiltersForm";

const FlightFiltersContainer: React.FC = () => {
  const { setFilters } = useStoreActions((actions) => actions.filters);
  const companiesPrices = useStoreState(
    (state) => state.flights.flightsCompaniesPrices
  );

  const onFiltersFormChange = useCallback(
    (values: IFiltersFormState) => {
      setFilters(values);
    },
    [setFilters]
  );

  const companiesList = useMemo(() => {
    return companiesPrices.map((item) => {
      let label = `${item.value.caption}`;
      if (item.price) {
        label = `${label} от ${item.price}`;
      }
      return {
        value: item.value.uid,
        label,
      };
    });
  }, [companiesPrices]);

  return (
    <FlightFilters
      companies={companiesList}
      onFiltersFormChange={onFiltersFormChange}
    />
  );
};

export default FlightFiltersContainer;
