import { useCallback, useState } from "react";

function useSelectedList<T>(state: T | T[] | undefined, multi = true) {
  const [selected, setSelected] = useState<T | T[] | undefined>(state as T);

  const addSelected = useCallback(
    (value: T) => {
      if (multi && Array.isArray(selected)) {
        const indexOfKey = selected.indexOf(value);
        if (indexOfKey !== -1) {
          setSelected(selected.filter((_, index) => index !== indexOfKey));
        } else {
          setSelected([...selected, value]);
        }
      } else {
        if (selected === value) {
          setSelected(undefined);
        } else {
          setSelected(value);
        }
      }
    },
    [multi, selected, setSelected]
  );

  return { selected, addSelected };
}

export default useSelectedList;
