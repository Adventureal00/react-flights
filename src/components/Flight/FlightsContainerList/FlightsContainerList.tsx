import React, { useCallback, useMemo, useState } from "react";
import { useStoreState } from "../../../store";
import styled from "styled-components";
import FlightCard from "../FlightCard/FlightCard";

const CardsListWrapper = styled.div``;

const CardsList = styled.ul`
  list-style-type: none;
`;

const CardsListItem = styled.li`
  &:not(:first-child) {
    margin-top: 28px;
  }
`;

const CardsListBottom = styled.div`
  display: flex;
  justify-content: center;
  padding: 12px 0;
`;

const MoreButton = styled.button``;

const ITEMS_PER_PAGE = 8;
const CONTAINER_BUTTON_TEXT = "Показать еще";

const FlightsContainerList: React.FC = () => {
  const filtersFlights = useStoreState((state) => state.flights.filtersFlights);
  const [lastIndex, setLastIndex] = useState(ITEMS_PER_PAGE);

  const loadMore = useCallback(() => {
    if (lastIndex + ITEMS_PER_PAGE < filtersFlights.length) {
      setLastIndex(lastIndex + ITEMS_PER_PAGE);
    } else {
      setLastIndex(filtersFlights.length);
    }
  }, [lastIndex, filtersFlights, setLastIndex]);

  const listContent = useMemo(() => {
    return filtersFlights.slice(0, lastIndex).map((item) => (
      <CardsListItem key={item.flightToken}>
        <FlightCard
          flightCost={parseInt(item.flight.price.totalFeeAndTaxes.amount)}
          flightLegs={item.flight.legs}
        />
      </CardsListItem>
    ));
  }, [filtersFlights, lastIndex]);

  return (
    <CardsListWrapper>
      <CardsList>{listContent}</CardsList>
      {filtersFlights.length > 0 && (
        <CardsListBottom>
          <MoreButton onClick={loadMore}>{CONTAINER_BUTTON_TEXT}</MoreButton>
        </CardsListBottom>
      )}
    </CardsListWrapper>
  );
};

export default FlightsContainerList;
