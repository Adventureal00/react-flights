import { IAirline } from "./IAirline";
import { IFlightLeg } from "./IFlightSegment";

export interface IFlightPrice {
  amount: string;
  currency: string;
  currencyCode: string;
}

export interface IFlight {
  flight: {
    carrier: IAirline;
    price: {
      total: IFlightPrice;
      totalFeeAndTaxes: IFlightPrice;
    };
    legs: IFlightLeg[];
  };
  flightToken: string;
}
