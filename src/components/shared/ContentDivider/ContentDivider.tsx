import React from "react";
import styled from "styled-components";
import { greyColor, orangeColor } from "../../../styles/colors";

const DividerWrapper = styled.div`
  display: flex;
  max-width: 100%;
  align-items: center;
  padding: 10px 40px;
  position: relative;
  text-align: center;
`;

const DividerBorder = styled.div`
  flex: 1;
  border-bottom: 1px solid ${greyColor};
`;

const DividerContent = styled.div`
  position: absolute;
  padding: 0 10px;
  color: ${orangeColor};
  background: #fff;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  font-size: 14px;
`;

interface ContentDividerProps {
  title: string;
  showTitle: boolean;
}

const ContentDivider: React.FC<ContentDividerProps> = (props) => {
  return (
    <DividerWrapper>
      <DividerBorder />
      {props.showTitle && <DividerContent>{props.title}</DividerContent>}
      <DividerBorder />
    </DividerWrapper>
  );
}

export default ContentDivider;
