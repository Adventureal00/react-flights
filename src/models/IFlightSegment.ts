import { IUidCaption } from "./IUidCaption";
import { IAirline } from "./IAirline";

export interface IFlightLegSegment {
  departureAirport: IUidCaption;
  arrivalAirport: IUidCaption;
  classOfService: IUidCaption;
  departureCity: IUidCaption;
  arrivalCity: IUidCaption;
  aircraft: IUidCaption;
  airline: IAirline;
  travelDuration: number;
  flightNumber: string;
  arrivalDate: string;
  departureDate: string;
  stops: number;
  operatingAirline?: IAirline;
}

export interface IFlightLeg {
  duration: number;
  segments: IFlightLegSegment[];
}
